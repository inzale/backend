
const jwt = require('jsonwebtoken');
const User = require('../models/User');

module.exports = authenticateToken = async (req, res, next) => {
  const authHeader = req.headers['authorization'];
  const token = authHeader && authHeader.split(' ')[1];

  if (!token) {
    return res.status(401).json({ error: 'Acceso no autorizado. Token no proporcionado.' });
  }

  try {
    const decoded = jwt.verify(token.toString(), process.env.JWT_SECRET);
    const user = await User.findById(decoded.userId);

    if (!user) {
      return res.status(401).json({ error: 'Acceso no autorizado. Usuario no encontrado.' });
    }

    req.user = user; // Agregar el usuario al objeto de solicitud para su uso posterior
    next();
  } catch (error) {
    return res.status(401).json({ error: 'Acceso no autorizado. Token no válido.' });
  }
};
