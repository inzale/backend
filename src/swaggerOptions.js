const swaggerJsdoc = require('swagger-jsdoc');
const path = require("path");
const options = {
  definition: {
    openapi: '3.0.3',
    info: {
      title: 'INLAZE SOCIAL',
      
      version: '1.0.0',
      description: 'Documentacion del API INLAZE SOCIAL',
    },


    components: {
      securitySchemes: {
          Authorization: {
              type: "http",
              scheme: "bearer",
              value: "<JWT token here>"
          }
      }
    }

  },
  apis: [`${path.join(__dirname,"./routes/*.js")}`], // Rutas de tus archivos que contienen la documentación JSDoc
};

const swaggerSpec = swaggerJsdoc(options);

module.exports = swaggerSpec;
