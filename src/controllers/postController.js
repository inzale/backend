// controllers Post
const Post = require('../models/Post');

/*
@param
page= numero de pagina pagesize= publicaciones por pagina
@return
Publicaciones junto con el tamaño de la pagina y publicaciones por pagina
*/
exports.getAllPosts = async (req, res) => {
  try {
    const page = parseInt(req.query.page) || 1;
    const pageSize = parseInt(req.query.pageSize) || 10; // Número de elementos por página

    const totalPosts = await Post.countDocuments({ deletedAt: { $exists: false } });
    const totalPages = Math.ceil(totalPosts / pageSize);

    const posts = await Post.find({ deletedAt: { $exists: false } })
      .sort({ createdAt: 'desc' })
      .skip((page - 1) * pageSize)
      .limit(pageSize);

    res.json({
      posts,
      currentPage: page,
      totalPages,
      totalPosts,
    });
  } catch (error) {
    console.error(error);
    next(error);
  }
};
/*
@param
title= Nombre de la publicacion, 
content= contenido de la publicacion, 
userId= id del usuario que hizo la publicacion 
@return
Devuelve la info de la publicacion
*/
exports.createPost = async (req, res) => {
  try {
    const { title, content, userId } = req.body;
    const newPost = new Post({ title, content, userId });
    await newPost.save();
    res.status(201).json(newPost);
  } catch (error) {
    console.error(error);
    next(error);
  }
};
/*
@param
title= Nombre de la publicacion, 
content= contenido de la publicacion, 
userId= id del usuario que hizo la publicacion 
@return
Devuelve la info de la publicacion
*/
exports.updatePost = async (req, res) => {
  try {
    const { title, content } = req.body;
    const postId = req.params.id;

    const post = await Post.findById(postId);
    if (!post) {
      return res.status(404).json({ error: 'Publicación no encontrada' });
    }

    post.title = title || post.title;
    post.content = content || post.content;
    post.updatedAt = Date.now();

    await post.save();
    res.json(post);
  } catch (error) {
    console.error(error);
    next(error);
  }
};

exports.deletePost = async (req, res) => {
  try {
    const postId = req.params.id;
    const post = await Post.findById(postId);

    if (!post) {
      return res.status(404).json({ error: 'Publicación no encontrada' });
    }

    post.deletedAt = Date.now();
    post.state =false
    await post.save();
    res.json({ message: 'Publicación eliminada correctamente' });
  } catch (error) {
    console.error(error);
    next(error);
  }
};
