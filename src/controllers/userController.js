// userController.js
const User = require('../models/User');

exports.getUserById = async (req, res, next) => {
  try {
    const userId = req.params.id;
    const user = await User.findById(userId).populate('posts');
    
    if (!user) {
      return res.status(404).json({ error: 'Usuario no encontrado' });
    }

    res.json(user);
  } catch (error) {
    console.error(error);
    next(error);
  }
};

exports.updateUser = async (req, res, next) => {
    try {
      const userId = req.params.id;
      const { fullName, age, email } = req.body;
  
      const user = await User.findById(userId);
      if (!user) {
        return res.status(404).json({ error: 'Usuario no encontrado' });
      }
  
      user.fullName = fullName || user.fullName;
      user.age = age || user.age;
      user.email = email || user.email;
      user.updatedAt = Date.now();
  
      await user.save();
      res.json(user);
    } catch (error) {
      console.error(error);
      next(error);
    }
  };