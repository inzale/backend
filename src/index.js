const express = require('express');
const mongoose = require('mongoose');
const dotenv = require('dotenv');
const { handleErrors } = require('./middlewares/errorMiddleware');
const authRoutes = require('./routes/authRoutes');
const postRoutes = require('./routes/PostRoutes');
const userRoutes = require('./routes/userRoutes');
const swaggerUi = require('swagger-ui-express');
const swaggerSpec = require('./swaggerOptions');
dotenv.config();

const app = express();
app.use(express.json());

// Conexion a la base de datos MongoDB
mongoose.connect(process.env.MONGODB_URI)
  .then(() => console.log('Conexión a MongoDB establecida'))
  .catch(err => console.error('Error al conectar a MongoDB:', err));

// Rutas
app.use('/api', authRoutes);
app.use('/api', postRoutes);
app.use('/api', userRoutes);

// Middleware de manejo de errores
app.use(handleErrors);

// Configuración de Swagger UI
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Servidor en ejecución en el puerto ${PORT}`);
});
