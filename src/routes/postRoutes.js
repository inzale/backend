/**
 * @swagger
 * tags:
 *   name: Posts
 *   description: Operaciones relacionadas con las publicaciones.
 */

/**
 *@swagger
 *components:
 *  schemas:
 *      Post:
 *          type: object
 *          properties:
 *              title:
 *                  type: string
 *                  description: Título de la publicación.
 *              content:
 *                  type: string
 *                  description: Contenido de la publicación.
 *              Like:
 *                  type: Integer
 *                  description: Cantidad de likes a la publicación.
 *              userId:
 *                  type: string
 *                  description: ID del usuario que creó la publicación.
 *              createdAt:
 *                  type: string
 *                  format: date-time
 *                  description: Fecha y hora de creación de la publicación.
 *              updatedAt:
 *                  type: string
 *                  format: date-time
 *                  description: Fecha y hora de la última actualización de la publicación.
 *              deletedAt:
 *                  type: string
 *                  format: date-time
 *                  description: Fecha y hora de la última eliminación de la publicación.
 
 
 *          required:
 *              -tittle
 *              -content
 *              -userId
 */

/**
 * @swagger
 * /api/posts:
 *   get:
 *     summary: Obtiene todas las publicaciones.
 *     tags: [Posts]
 *     responses:
 *       '200':
 *         description: Respuesta exitosa.
 *         content:
 *           application/json:
 *             example:
 *               message: Lista de publicaciones obtenida con éxito.
 *               posts:
 *                 - _id: '123'
 *                   title: 'Publicación 1'
 *                   content: 'Contenido de la publicación 1'
 *                   createdAt: '2022-01-01T12:00:00Z'
 *                   updatedAt: '2022-01-01T12:30:00Z'
 *                 - _id: '456'
 *                   title: 'Publicación 2'
 *                   content: 'Contenido de la publicación 2'
 *                   createdAt: '2022-01-02T14:00:00Z'
 *                   updatedAt: '2022-01-02T14:30:00Z'
 */
const express = require('express');
const postController = require('../controllers/postController');
const authenticateToken = require('../middlewares/authMiddleware');

const router = express.Router();

// Rutas públicas

/**
 * @swagger
 * /api/posts:
 *   get:
 *     summary: Obtiene todas las publicaciones.
 *     tags: [Posts]
 *     responses:
 *       '200':
 *         description: Respuesta exitosa.
 *         content:
 *           application/json:
 *             example:
 *               message: Lista de publicaciones obtenida con éxito.
 *               posts:
 *                 - _id: '123'
 *                   title: 'Publicación 1'
 *                   content: 'Contenido de la publicación 1'
 *                   createdAt: '2022-01-01T12:00:00Z'
 *                   updatedAt: '2022-01-01T12:30:00Z'
 *                 - _id: '456'
 *                   title: 'Publicación 2'
 *                   content: 'Contenido de la publicación 2'
 *                   createdAt: '2022-01-02T14:00:00Z'
 *                   updatedAt: '2022-01-02T14:30:00Z'
 */
router.get('/posts', postController.getAllPosts);

// Rutas protegidas
/**
 * @swagger
 * /api/posts:
 *   post:
 *     summary: Crea una nueva publicación.
 *     security:
*       - Authorization: []
 *     tags: [Posts]
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: ID del usuario que coloca la publicacion.
 *         schema:
 *           type: string
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               title:
 *                 type: string
 *                 description: Título de la publicación.
 *               content:
 *                 type: string
 *                 description: Contenido de la publicación.
 *     responses:
 *       '201':
 *         description: Publicación creada exitosamente.
 *         content:
 *           application/json:
 *             example:
 *               message: Publicación creada con éxito.
 *               post:
 *                 _id: '789'
 *                 title: 'Nueva Publicación'
 *                 content: 'Contenido de la nueva publicación'
 *                 userId: '123'
 *                 createdAt: '2022-01-03T15:00:00Z'
 *                 updatedAt: null
 *       '401':
 *         description: No autorizado. Token no proporcionado o inválido.
 */
router.post('/posts', authenticateToken, postController.createPost);
/**
 * @swagger
 * /api/posts/{id}:
 *   put:
 *     summary: Actualiza una publicación.
 *     security:
*       - Authorization: []
 *     tags: [Posts]
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: ID de la publicacion que se va a modificar.
 *         schema:
 *           type: string
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               title:
 *                 type: string
 *                 description: Título de la publicación.
 *               content:
 *                 type: string
 *                 description: Contenido de la publicación.

 *     responses:
 *       '201':
 *         description: Publicación actualizada exitosamente.
 *         content:
 *           application/json:
 *             example:
 *               message: Publicación actualizada con éxito.
 *               post:
 *                 _id: '789'
 *                 title: 'Actualizar Publicación'
 *                 content: 'Contenido de la nueva publicación'
 *                 userId: '123'
 *                 createdAt: '2022-01-03T15:00:00Z'
 *                 updatedAt: '2022-01-03T15:00:00Z'
 *       '401':
 *         description: No autorizado. Token no proporcionado o inválido.
 */
router.put('/posts/:id', authenticateToken, postController.updatePost);
/**
 * @swagger
 * /api/posts/{id}:
 *   delete:
 *     summary: Elimina una publicación por ID.
 *     security:
*       - Authorization: []
 *     tags: [Posts]
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: ID de la publicación que se va a eliminar.
 *         schema:
 *           type: string
 *     responses:
 *       '200':
 *         description: Publicación eliminada exitosamente.
 *         content:
 *           application/json:
 *             example:
 *               message: Publicación eliminada con éxito.
 *       '401':
 *         description: No autorizado. Token no proporcionado o inválido.
 *       '404':
 *         description: Publicación no encontrada.
 */
router.delete('/posts/:id', authenticateToken, postController.deletePost);

module.exports = router;
