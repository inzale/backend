const express = require('express');
const authController = require('../controllers/authController');
const router = express.Router();

//Rutas de inicio y registro de Usuario
/**
 * @swagger
 * tags:
 *   name: Authentication
 *   description: Operaciones relacionadas con la autenticación de usuarios.
 */

/**
 * @swagger
 * /api/auth/register:
 *   post:
 *     summary: Registra un usuario.
 *     tags: [Authentication]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               fullName:
 *                 type: string
 *                 description: Nombre completo del usuario.
 *               age:
 *                type: Integer
 *                description: Edad del usuario
 *               email:
 *                 type: string
 *                 description: Correo electrónico del usuario.
 *               password:
 *                 type: string
 *                 description: Contraseña del usuario.
 *     responses:
 *       '200':
 *         description: Registro exitoso. Se devuelve un token de acceso.
 *         content:
 *           application/json:
 *             example:
 *               message: Registro exitoso.
 *               token: 'eyJhbGciOiJIUzI1NiIsIn... (token JWT)'
 *       '401':
 *         description: Error al registrar usuario.
 */
router.post('/auth/register', authController.register);

/**
 * @swagger
 * /api/auth/login:
 *   post:
 *     summary: Inicia sesión y obtiene un token de acceso.
 *     tags: [Authentication]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               email:
 *                 type: string
 *                 description: Correo electrónico del usuario.
 *               password:
 *                 type: string
 *                 description: Contraseña del usuario.
 *     responses:
 *       '200':
 *         description: Inicio de sesión exitoso. Se devuelve un token de acceso.
 *         content:
 *           application/json:
 *             example:
 *               message: Inicio de sesión exitoso.
 *               token: 'eyJhbGciOiJIUzI1NiIsIn... (token JWT)'
 *       '401':
 *         description: Credenciales inválidas. Inicio de sesión no autorizado.
 */
router.post('/auth/login', authController.login);

module.exports = router;