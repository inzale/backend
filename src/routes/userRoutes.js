/**
 * @swagger
 * tags:
 *   name: Users
 *   description: Operaciones relacionadas con los usuarios.
 */

/**
 *@swagger
 *components:
 *  schemas:
 *      User:
 *          type: object
 *          properties:
 *              fullName:
 *                  type: string
 *                  description: el nombre del usuario
 *              age:
 *                  type: integer
 *                  description: edad del usuario
 *              email:
 *                  type: string
 *                  description: email del usuario
 *              createdAt:
 *                  type: string
 *                  format: date-time
 *                  description: Fecha y hora de creación del usuario.
 *              updatedAt:
 *                  type: string
 *                  format: date-time
 *                  description: Fecha y hora de la última actualización del usuario.
 *              deletedAt:
 *                  type: string
 *                  format: date-time
 *                  description: Fecha y hora de la eliminacion del usuario.
 
 *          required:
 *              -fullName
 *              -age
 *              -email
 */
const express = require('express');
const userController = require('../controllers/userController');
const authenticateToken = require('../middlewares/authMiddleware');

const router = express.Router();

// Rutas protegidas
/**
 * @swagger
 * /api/users/{id}:
 *   get:
 *     summary: Obtiene información de un usuario por ID.
 *     tags: [Users]
 *     security:
*       - Authorization: []
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: ID del usuario.
 *         schema:
 *           type: string
 *     responses:
 *       '200':
 *         description: Respuesta exitosa.
 *         content:
 *           application/json:
 *             example:
 *               user:
 *                 _id: '123'
 *                 fullName: 'John Doe'
 *                 age: 25
 *                 email: 'john@example.com'
 *                 posts: []
 *                 createdAt: '2022-01-03T12:00:00Z'
 *                 updatedAt: null
 *       '401':
 *         description: No autorizado. Token no proporcionado o inválido.
 *       '404':
 *         description: Usuario no encontrado.
 */
router.get('/users/:id', authenticateToken, userController.getUserById);
/**
 * @swagger
 * /api/users/{id}:
 *   put:
 *     summary: Actualizar un usuario.
 *     security:
*       - Authorization: []
 *     tags: [Users]
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: ID del usuario que se va a modificar.
 *         schema:
 *           type: string
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               fullName:
 *                 type: string
 *                 description: Nombre completo del usuario.
 *               age:
 *                 type: number
 *                 description: Edad del usuario.
 *               email:
 *                 type: string
 *                 description: Correo electrónico del usuario.
 *               password:
 *                 type: string
 *                 description: Contraseña del usuario.
 *     responses:
 *       '201':
 *         description: Usuario modificado exitosamente.
 *         content:
 *           application/json:
 *             example:
 *               message: Usuario modificado con éxito.
 *               user:
 *                 _id: '456'
 *                 fullName: 'Jane Doe'
 *                 age: 30
 *                 email: 'jane@example.com'
 *                 posts: []
 *                 updatedAt:  '2022-01-03T14:00:00Z'
 *       '400':
 *         description: Solicitud incorrecta. Verifica los parámetros enviados.
 */
router.put('/users/:id', userController.updateUser);

module.exports = router;